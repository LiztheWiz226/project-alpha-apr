from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(False)
            task.creator = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    others_tasks = Task.objects.filter(creator=request.user).exclude(
        assignee=request.user
    )
    team_tasks = Task.objects.exclude(assignee=request.user)
    context = {
        "my_tasks": my_tasks,
        "others_tasks": others_tasks,
        "team_tasks": team_tasks,
    }
    return render(request, "tasks/list.html", context)


@login_required
def edit_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskForm(instance=task)
    context = {
        "task": task,
        "form": form,
    }
    return render(request, "tasks/create.html", context)
